# Platyca Dynamic

## Build

```
sudo apt install libssl-dev python3-certifi
cd .../src
make clean
make
```

# Run

In `screen` command:

```
cd .../src
./platyca
```

```
cd .../src
./platyca-registerer
```

```
cd .../caddy
sudo caddy run
```

# Acknowledgements

https://github.com/cesanta/mongoose

https://cesanta.com/docs/
