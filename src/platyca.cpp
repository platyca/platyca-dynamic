#include <set>
#include <map>
#include <algorithm>
#include <vector>
#include <ctime>
#include <iostream>
#include <cstdlib>
#include <deque>

#include "mongoose.h"


using namespace std;


/* Global */


struct mg_mgr mongoose_manager;

set <string> peers {
	string {"http://platyca.tk/"},
	string {"http://register.platyca.tk/"},
	string {"http://todes.platyca.tk/"},
	string {"http://platyca.gitlab.io/platyca/mary/"},
};

set <string> hidden_peers;

set<string> board_names {
	string {"http://platyca.tk/boards/messages/"},
};

map <string, map <string, vector <string>>> boards;

deque <string> explore_queue;


/* Util */

static string make_string (struct mg_str in)
{
	return string {in.ptr, in.len};
}


/* Client */


struct ClientAnything {
	string url;
	string peer;
};


static vector <vector <string>> decode_csv (string csv)
{
	vector <vector <string>> table;
	vector <string> record;
	string field;

	uint64_t state = 0;

	for (auto c: csv) {
		switch (state) {
		case 0:
			if (c == '"') {
				state = 1;
			} else if (c == ',') {
				record.push_back (field);
				field.clear ();
				state = 0;
			} else if (c == '\n') {
				record.push_back (field);
				field.clear ();
				table.push_back (record);
				record.clear ();
				state = 0;
			} else if (c == '\r') {
				/* Do nothing. */
			} else {
				field.push_back (c);
			}
			break;
		case 1:
			if (c == '"') {
				state = 2;
			} else if (c == '\r') {
				/* Do nothing. */
			} else {
				field.push_back (c);
			}
			break;
		case 2:
			if (c == '"') {
				field.push_back ('"');
				state = 1;
			} else if (c == ',') {
				record.push_back (field);
				field.clear ();
				state = 0;
			} else if (c == '\n') {
				record.push_back (field);
				field.clear ();
				table.push_back (record);
				record.clear ();
				state = 0;
			} else if (c == '\r') {
				/* Do nothing. */
			} else {
				field.push_back (c);
				state = 0;
			}
			break;
		default:
			abort ();
			break;
		}
	}
	
	if (! field.empty ()) {
		record.push_back (field);
	}
	
	if (! record.empty ()) {
		table.push_back (record);
	}
	
	return table;
}


static void push_to_explore_queue_impl (string peer)
{
	if (find (explore_queue.begin (), explore_queue.end (), peer) == explore_queue.end ()) {
		explore_queue.push_back (peer);
	}
}


static void push_to_explore_queue (string peer)
{
	if (peer != string {"SLEEP"}) {
		push_to_explore_queue_impl (peer);
	}
}


static void push_sleep_to_explore_queue ()
{
	push_to_explore_queue_impl (string {"SLEEP"});
}


static void collect_peers (string peer);


static void client_call_back (struct mg_connection *c, int ev, void *ev_data, void *fn_data) {
	ClientAnything * client_anything = reinterpret_cast <ClientAnything *> (fn_data);

	const char *url = client_anything->url.c_str ();

	if (ev == MG_EV_CONNECT) {
		struct mg_str host = mg_url_host (url);
		if (mg_url_is_ssl (url)) {
			struct mg_tls_opts opts = {.ca = "/usr/lib/python3/dist-packages/certifi/cacert.pem"};
			mg_tls_init (c, &opts);
		}
		mg_printf (
			c,
			"GET %s HTTP/1.0\r\nHost: %.*s\r\n\r\n",
			mg_url_uri (url),
			(int) host.len,
			host.ptr
		);
	} else if (ev == MG_EV_HTTP_MSG) {
		cerr << "OK " << client_anything->url << endl;
	
		for (auto &name_and_board: boards) {
			map <string, vector <string>> &board = name_and_board.second;
			if (board.find (client_anything->peer) != board.end ()) {
				board.erase (client_anything->peer);
			}
		}

		struct mg_http_message *hm = reinterpret_cast <struct mg_http_message *> (ev_data);
		string body = make_string (hm->body);

		auto table = decode_csv (body);
		
		for (auto record: table) {
			if (0 < record.size ()) {
				string type = record.at (0);
				if (type == string {"peer"} && 1 < record.size ()) {
					push_to_explore_queue (record.at (1));
				} else if (type == string {"entry"} && 2 < record.size ()) {
					string board_name = record.at (1);
					string text = record.at (2);
					
					if (boards.find (board_name) != boards.end ()) {
						map <string, vector <string>> &board = boards.at (board_name);
						if (board.find (client_anything->peer) == board.end ()) {
							board.insert (pair <string, vector <string>> {
								client_anything->peer,
								vector <string> {text}
							});
						} else {
							board.at (client_anything->peer).push_back (text);
						}
					}
				}
			}
		}

		push_to_explore_queue (client_anything->peer);

		delete client_anything;
		c->is_closing = 1;
	}
}


static void collect_peers (string peer)
{
	cerr << "Peer " << peer << endl;

	string url = peer + string {"peers.csv"};

	auto client_anything = new ClientAnything ;
	client_anything->peer = peer;
	client_anything->url = url;

	mg_http_connect (
		& mongoose_manager,
		url.c_str (),
		client_call_back,
		reinterpret_cast <void *> (client_anything)
	);
}


static void explore_task ()
{
	for (auto peer: peers) {
		push_to_explore_queue (peer);
	}

	for (auto peer: hidden_peers) {
		push_to_explore_queue (peer);
	}
	
	push_sleep_to_explore_queue ();
	
	auto front = explore_queue.front ();
	explore_queue.pop_front ();
	
	collect_peers (front);
}


/* Server */


static bool by_length (string left, string right)
{
	return left.size () < right.size ();
}


static string escape_attribute (string in)
{
	string out;
	for (auto c: in) {
		if (c == '"') {
			out += string {"&quot;"};
		} else if (c == '&') {
			out += string {"&amp;"};
		} else {
			out.push_back (c);
		}
	}
	return out;
}


static string escape_html (string in)
{
	string out;
	for (auto c: in) {
		if (c == '<') {
			out += string {"&lt;"};
		} else if (c == '&') {
			out += string {"&amp;"};
		} else {
			out.push_back (c);
		}
	}
	return out;
}


static string escape_csv (string in)
{
	string out;
	for (auto c: in) {
		if (c == '"') {
			out += string {"\"\""};
		} else {
			out.push_back (c);
		}
	}
	return out;
}


static bool starts_with (string x, string y)
{
	return
		y.size () <= x.size ()
		&& x.substr (0, y.size ()) == y;
}


static bool ends_with (string x, string y)
{
	return
		y.size () <= x.size ()
		&& x.substr (x.size () - y.size ()) == y;
}


static string show_text (vector <string> text, string peer)
{
	string html;

	for (auto line: text) {
		html += string {"<p>"};
		if (
			starts_with (line, string {"http://"})
			|| starts_with (line, string {"https://"})
		) {
			if (
				ends_with (peer, "/")
				&& starts_with (line, peer)
				&& (
					ends_with (line, string {".png"})
					|| ends_with (line, string {".jpg"})
					|| ends_with (line, string {".jpeg"})
					|| ends_with (line, string {".gif"})
					|| ends_with (line, string {".svg"})
					|| ends_with (line, string {".webp"})
				)
			) {
				html
					+= "<a href=\""
					+ escape_attribute (line)
					+ "\" target=\"_blank\">"
					+ escape_html (line)
					+ "</a></br>"
					+ "<img src=\""
					+ escape_attribute (line)
					+ "\" style=\"width: 16em; height: 16em; object-fit: contain; border: solid thin gray; \">";
			} else {
				html
					+= "<a href=\""
					+ escape_attribute (line)
					+ "\" target=\"_blank\">"
					+ escape_html (line)
					+ "</a>";
			}
		} else {
			html += escape_html (line);
		}
		html += string {"</p>\r\n"};
	}

	return html;
}


static string show_board (map <string, vector <string>> board)
{
	vector <string> peers;
	for (auto i: board) {
		peers.push_back (i.first);
	}
	stable_sort (peers.begin (), peers.end (), by_length);

	string html;
	
	for (string peer: peers) {
		vector <string> text = board.at (peer);
		html
			+= string {"<h3>"}
			+ string {"<img src=\""}
			+ escape_attribute (peer + string {"favicon.ico"})
			+ string {"\" style=\"width:1.5em; height;1.5em; vertical-align:middle;\" "}
			+ string {"onerror=\"this.src='/peer.png'\""}
			+ string {">"}
			+ string {" "}
			+ string {"<a href=\""}
			+ escape_attribute (peer)
			+ string {"\" target=\"_blank\">"}
			+ escape_html (peer)
			+ string {"</a></h3>\r\n"};
		html += show_text (text, peer);
	}
	
	return html;
}



static string construct_messages_page ()
{
	string html = string {
		"<!DOCTYPE html>\r\n"
		"<base href=\"http://platyca.tk/\">\r\n"
		"<meta charset=\"utf-8\">\r\n"
		"<meta name=\"viewport\" content=\"width=device-width\">\r\n"
		"<title>Platica Dynamic &gt; Messages</title>\r\n"
		"<meta property=\"og:title\" content=\"Messages\" />\r\n"
		"<meta property=\"og:type\" content=\"website\" />\r\n"
		"<meta property=\"og:url\" content=\"http://platyca.tk/board/messages/\" />\r\n"
		"<meta property=\"og:image\" content=\"http://platyca.tk/og.jpeg\" />\r\n"
		"<meta property=\"og:description\" content=\"分散掲示板ネットワーク\" />\r\n"
		"<meta property=\"og:site_name\" content=\"Platica Dynamic\" />\r\n"
		"<p><a href=\"/\">Platyca Dynamic</a></p>\r\n"
		"<h1>Messages</h1>\r\n"
	};

	map <string, vector <string>> board = boards.at (string {"http://platyca.tk/boards/messages/"});

	html += show_board (board);
	
	return html;
}


static string construct_peers_page () {
	string csv;
	
	vector <string> peers_vector {peers.begin (), peers.end ()};
	stable_sort (peers_vector.begin (), peers_vector.end (), by_length);

	for (string peer: peers_vector) {
		csv += string {"\"peer\",\""} + escape_csv (peer) + string {"\"\r\n"};
	}

	for (string board_name: board_names) {
		csv += string {"\"board\",\""} + board_name + string {"\"\r\n"};
	}
	return csv;
};


static string construct_404_page ()
{
	string html = string {
		"<!DOCTYPE html>\r\n"
		"<meta name=\"viewport\" content=\"width=device-width\">\r\n"
		"<title>404</title>\r\n"
		"<p>404</p>\r\n"
	};

	return html;
}


static void cb (struct mg_connection *c, int ev, void *ev_data, void *fn_data) {
	if (ev == MG_EV_HTTP_MSG) {
		auto http_message = reinterpret_cast <struct mg_http_message *> (ev_data);
		if (make_string (http_message->method) == string {"GET"}) {
			if (make_string (http_message->uri) == string {"/"}) {
				mg_http_serve_file (
					c,
					http_message,
					"index.html",
					"text/html"
				);
			} else if (make_string (http_message->uri) == string {"/peers.csv"}) {
				mg_http_reply (
					c,
					200,
					"Content-Type: text/plain\r\n",
					"%s",
					construct_peers_page ().c_str ()
				);
			} else if (make_string (http_message->uri) == string {"/boards/messages/"}) {
				mg_http_reply (
					c,
					200,
					"Content-Type: text/html\r\n",
					"%s",
					construct_messages_page ().c_str ()
				);
			} else if (make_string (http_message->uri) == string {"/favicon.ico"}) {
				mg_http_serve_file (
					c,
					http_message,
					"example-5550-crop.png",
					"image/png"
				);
			} else if (make_string (http_message->uri) == string {"/og.jpeg"}) {
				mg_http_serve_file (
					c,
					http_message,
					"example-5550-crop.jpg",
					"image/jpeg"
				);
			} else if (make_string (http_message->uri) == string {"/peer.png"}) {
				mg_http_serve_file (
					c,
					http_message,
					"1F310_color.png",
					"image/png"
				);
			} else {
				mg_http_reply (
					c,
					404,
					"Content-Type: text/html\r\n",
					"%s",
					construct_404_page ().c_str ()
				);
			}
		}
	}
}


/* Main */


int main (int argc, char *argv []) {
	for (string board_name: board_names) {
		boards.insert (pair <string, map <string, vector <string>>> {
			board_name,
			map <string, vector <string>> {}
		});
	}

	mg_mgr_init (& mongoose_manager);
	mg_http_listen (& mongoose_manager, "http://localhost:8000", cb, nullptr);

	time_t timer = time (nullptr);

	for (; ; ) {
		mg_mgr_poll (& mongoose_manager, 100);
		if (0 < explore_queue.size () && explore_queue.front () == string {"SLEEP"}) {
			if (timer + 60 < time (nullptr)) {
				explore_queue.pop_front ();
				timer = time (nullptr);
			}
		} else {
			explore_task ();
		}
	}

	mg_mgr_free (& mongoose_manager);
	return 0;
}

